﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rotate_Script : MonoBehaviour
{

    public float m_RotSpeed = 3f;
    public Quaternion t_RotStart;
    Quaternion t_Rotation;
    public Rigidbody m_Rb;
    public LayerMask m_UnSelectLayer;
    public bool b_IsActive = true;


    public bool b_Top = false;
    public bool b_Left = false;


    Vector2 t_Temp;
    Vector2 t_Cur;

    float t_PrevMouse;
    float t_CurMouse;
    float t_Delta;
    Vector2 t_Pos;
    Vector2 t_Del;

    public Text t_Text;

    float t_RotX;
    float t_RotY;


    private void Start() {
        t_RotStart = transform.rotation;
    }

    private void Update() {
//        if (b_IsActive) {

//            if (Input.GetMouseButtonUp(0)) {
//                b_Top = false;
//                b_Left = false;
//            }
//            if (Input.GetMouseButton(0)) {
//                if (Physics2D.CircleCast(Input.mousePosition, 10, Vector2.up, 50)) {
//                    return;
//                }

//#if !UNITY_EDITOR
//                if (Input.GetAxis("Mouse Y") > 1 || Input.GetAxis("Mouse Y") < -1 && b_Top == false) {
//                    b_Left = true;
//                    t_Rotation = Quaternion.Euler(Input.GetTouch(0).deltaPosition.y * m_RotSpeed, 0, 0f);
//                t_Text.text = "Atas";
//                }
//                if (Input.GetAxis("Mouse X") > 1 || Input.GetAxis("Mouse X") < -1 && b_Left == false) {
//                    b_Top = true;
//                    t_Rotation = Quaternion.Euler(Input.GetTouch(0).deltaPosition.x, -Input.GetTouch(0).deltaPosition.x * m_RotSpeed, 0f);
//                t_Text.text = "Bawah";

//                }
                
//                //t_Rotation = Quaternion.Euler(Input.GetTouch(0).deltaPosition.y * m_RotSpeed, -Input.GetTouch(0).deltaPosition.x * m_RotSpeed, 0f);
//                transform.rotation = t_Rotation * transform.rotation;
//#endif

//            }
//        }
    }


    private void OnMouseDrag() {
        t_RotX = Input.GetAxis("Mouse X") * m_RotSpeed;
        t_RotY = Input.GetAxis("Mouse Y") * m_RotSpeed;
        //transform.Rotate(Vector3.down, t_RotX);
        //transform.Rotate(Vector3.right, t_RotY);
        if (Input.GetAxis("Mouse X") > 0.3f || Input.GetAxis("Mouse X") < -0.3f ) {
            if (b_Top == false) {
                b_Left = true;
                transform.rotation = Quaternion.Euler(0, -t_RotX * m_RotSpeed, 0f) * transform.rotation;
            }
        }
        if (Input.GetAxis("Mouse Y") > 0.3f || Input.GetAxis("Mouse Y") < -0.3f ) {
            if(b_Left == false) {
                b_Top = true;
                transform.rotation = Quaternion.Euler(t_RotY * m_RotSpeed, 0, 0f) * transform.rotation;

            }
        }

    }

    private void OnMouseUp() {
        b_Top = false;
        b_Left = false;

    }

    public void ResetBox() {
        transform.rotation = t_RotStart;
    }
}

//if(Input.GetMouseButtonUp(0)){
//    transform.GetChild(0).localRotation = Quaternion.EulerAngles(transform.GetChild(0).localRotation.x + transform.localRotation.x, transform.GetChild(0).localRotation.y + transform.localRotation.y, transform.GetChild(0).localRotation.z + transform.localRotation.z);
//    transform.localRotation = Quaternion.identity;
//    t_Pos = Vector2.zero;
//}
//if (Input.GetMouseButtonDown(0)) {
//    t_Temp = Input.mousePosition;
//    t_Pos += t_Temp;
//}
//if (Input.GetMouseButton(0)) {
//    if (Physics2D.CircleCast(Input.mousePosition, 10, Vector2.up, 50)) {
//        return;
//    }
//    t_Cur = Input.mousePosition;
//    t_Del = t_Pos - t_Cur;
//    t_Rotation = Quaternion.Euler(-t_Del.y * m_RotSpeed * Time.deltaTime, t_Del.x * m_RotSpeed * Time.deltaTime, 0f);
//    transform.rotation = t_Rotation * transform.rotation;

//}